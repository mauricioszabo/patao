#!/bin/bash

lein uberjar && \
lein with-profile +java javac && \
$JAVA_HOME/bin/native-image  \
  --shared \
  --no-fallback \
  -jar target/big-duck-0.1.0-SNAPSHOT-standalone.jar \
  -cp target/classes/ \
  -H:+ReportExceptionStackTraces \
  -H:Name=libexample \
  -J-Dclojure.spec.skip-macros=true \
  -J-Dclojure.compiler.direct-linking=true \
  -H:ReflectionConfigurationFiles=reflection.json \
  --features=clj_easy.graal_build_time.InitClojureClasses &&
  mv graal_isolate* libexample* ruby/
