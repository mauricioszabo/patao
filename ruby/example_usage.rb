require_relative './patao'

patao = Patao.new
patao.gen_resolver( 'hello', output: [:foo__bar], input: []) do |inputs|
  {foo__bar: 10}
end
patao.gen_resolver( 'hello2', output: [:foo__lol], input: []) do |inputs|
  {foo__lol: 90}
end

patao2 = Patao.new
patao2.gen_resolver( 'hello', output: [:foo__bar], input: []) do |inputs|
  {foo__bar: 990}
end
patao2.gen_resolver( 'hello2', output: [:foo__lol], input: [:foo__bar]) do |inputs|
  {foo__lol: "New Input: " + inputs[:foo__bar].to_s}
end

p [:patao, patao.query([:foo__bar])]
p [:patao2, patao2.query([:foo__bar])]
p [:patao, patao.query([:foo__lol])]
p [:patao2, patao2.query([:foo__lol])]
