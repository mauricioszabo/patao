require "transit"
require_relative './example'

class Patao < Example
  def initialize
    super
    @blocks = []
  end

  def gen_resolver(name, params, &b)
    new_proc = proc do |i|
      to_str b.call(from_str(i))
    end
    @blocks << new_proc
    super(name, to_str(params), new_proc)
    gen_eql
  end

  def query(eql)
    res = super(to_str(eql))
    from_str res
  end

  def to_str(obj)
    io = StringIO.new('', 'w+')
    writer = Transit::Writer.new(:json, io)
    writer.write(obj)
    io.string.tap { io.close }
  end

  def from_str(str)
    reader = Transit::Reader.new(:json, StringIO.new(str))
    reader.read
  end
end
