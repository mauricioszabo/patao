package big_duck;

import org.graalvm.nativeimage.IsolateThread;
import org.graalvm.nativeimage.c.function.*;
// import org.graalvm.nativeimage.c.struct.RawPointerTo;
import org.graalvm.nativeimage.c.type.*;
import org.graalvm.word.*;

import java.util.function.Function;

public final class LibPatao {
  @CEntryPoint(name = "receive")
  public static CCharPointer receive(IsolateThread thread, CCharPointer s) {
    String expr = CTypeConversion.toJavaString(s);
    String result = big_duck.core.receive(expr);
    CTypeConversion.CCharPointerHolder holder = CTypeConversion.toCString(result);
    CCharPointer value = holder.get();
    return value;
  }

  @CEntryPoint(name = "gen_resolver")
  public static void gen_resolver(IsolateThread thread, Callback callback, VoidPointer arg1, CCharPointer name, CCharPointer params) {
    Function<String, String> adaptedCallback = (argument) -> {
      CTypeConversion.CCharPointerHolder holder = CTypeConversion.toCString(argument);
      CCharPointer value = holder.get();
      CCharPointer result = callback.invoke(arg1, value);
      String r = CTypeConversion.toJavaString(result);
      return r;
    };

    Object result = big_duck.core.gen_resolver(
      adaptedCallback,
      CTypeConversion.toJavaString(name),
      CTypeConversion.toJavaString(params)
    );
  }

  @CEntryPoint(name = "gen_eql")
  public static void gen_eql(IsolateThread thread) {
    big_duck.core.gen_eql();
  }

  @CEntryPoint(name = "query")
  public static CCharPointer query(IsolateThread thread, CCharPointer name) {
    String result = big_duck.core.query(CTypeConversion.toJavaString(name));
    CTypeConversion.CCharPointerHolder holder = CTypeConversion.toCString(result);
    CCharPointer value = holder.get();
    return value;
  }

  interface Callback extends CFunctionPointer {
    @InvokeCFunctionPointer
    CCharPointer invoke(VoidPointer arg1, CCharPointer output);
  }

  public static void main(String[] a) {
    System.out.println("OK, I guess " + a);
  }
}
