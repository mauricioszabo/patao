#include <ruby.h>
#include <libexample.h>

typedef struct {
    graal_isolate_t *isolate;
    graal_isolatethread_t *thread;
} RubyClassData;

void free_struct(RubyClassData* self) {
  graal_tear_down_isolate(self->thread);
  self->isolate = NULL;
  self->thread = NULL;
  free(self);
  self = NULL;
}

static VALUE ruby_class_allocate(VALUE klass) {
    RubyClassData *data = malloc(sizeof(RubyClassData));
    if (graal_create_isolate(NULL, &data->isolate, &data->thread) != 0) {
      fprintf(stderr, "initialization error\n");
      return 1;
    }
    return Data_Wrap_Struct(klass, NULL, free, data);
}

static graal_isolatethread_t* get_thread(VALUE self) {
    RubyClassData *data;
    Data_Get_Struct(self, RubyClassData, data);
    return data->thread;
}

const char* call_function(VALUE block, const char* str_from_clojure) {
  VALUE str_param = rb_str_new2(str_from_clojure);
  printf("Calling with %s\n", str_from_clojure);
  VALUE ret1 = rb_funcall(block, rb_intern("to_s"), 0);
  printf("OBJ: %s\n", StringValueCStr(ret1));
  VALUE ret = rb_funcall(block, rb_intern("call"), 1, str_param);
  printf("Called! %s\n", str_from_clojure);
  const char* result = StringValueCStr(ret);
  return result;
};

static VALUE example_gen_resolver(VALUE self, VALUE name, VALUE params, VALUE block) {
  void* callback = &call_function;
  gen_resolver(
    get_thread(self),
    callback,
    block,
    StringValueCStr(name),
    StringValueCStr(params)
  );
  return Qnil;
}

static VALUE example_gen_eql(VALUE self) {
  gen_eql(get_thread(self));
  return Qnil;
}

static VALUE example_query(VALUE self, VALUE q) {
  const char* q2 = StringValueCStr(q);
  const char* result = query(get_thread(self), q2);
  return rb_str_new2(result);
}

void Init_example() {
  // if (graal_create_isolate(NULL, &isolate, &thread) != 0) {
  //   fprintf(stderr, "initialization error\n");
  //   return 1;
  // }
  //
  VALUE ExampleClass = rb_define_class("Example", rb_cObject);
  rb_define_alloc_func(ExampleClass, ruby_class_allocate);
  rb_define_method(ExampleClass, "gen_resolver", example_gen_resolver, 3);
  rb_define_method(ExampleClass, "gen_eql", example_gen_eql, 0);
  rb_define_method(ExampleClass, "query", example_query, 1);
}
