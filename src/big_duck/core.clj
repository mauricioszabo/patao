(ns big-duck.core
  (:require [cognitect.transit :as transit]
            [com.wsscode.pathom3.connect.indexes :as indexes]
            [com.wsscode.pathom3.interface.eql :as eql]
            [com.wsscode.pathom3.connect.operation :as pco])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream]))

(gen-class
 :name "big_duck.core"
 :methods [^:static [receive [String] String]
           ^:static [gen_resolver [java.util.function.Function String String] Object]
           ^:static [gen_eql [] clojure.lang.IFn]
           ^:static [query [String] String]])

(defn- from-string [^java.lang.String string]
  (let [in (ByteArrayInputStream. (.getBytes string))
        reader (transit/reader in :json)]
    (transit/read reader)))

(defn- ^java.lang.String to-string [obj]
  (let [out (ByteArrayOutputStream.)
        writer (transit/writer out :json)]
    (transit/write writer obj)
    (.toString out)))

(defn -receive [x]
  (to-string (println "hello, world!" (from-string x))))

(defn- make-resolver [resolver-name params fun]
  (let [params (->> params
                    (map (fn [[k v]] [(keyword "com.wsscode.pathom3.connect.operation" (name k))
                                      v]))
                    (into {}))
        resolver-fun (fn [_env input]
                       (let [result (fun (to-string input))]
                         (from-string result)))]
    (pco/resolver (symbol resolver-name) params resolver-fun)))

(def ^:private resolvers (atom []))

(defn- -gen_resolver [^java.util.function.Function fun name params]
  (swap! resolvers
         conj
         (make-resolver name (from-string params) #(.apply fun %))))

(def ^:private eql (atom nil))

(defn- -gen_eql []
  (let [env (-> @resolvers
                indexes/register)]
    (reset! eql
            (fn eql [query]
              (let [res
                    (eql/process (-> env
                                     (assoc :com.wsscode.pathom3.error/lenient-mode? true))
                                 ; (pcp/with-plan-cache plan-cache)
                                 {}
                                 query)]
                res)))))

(defn- -query [str]
  (to-string (@eql (from-string str))))
