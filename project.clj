(defproject big-duck "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.cognitect/transit-clj "1.0.333"]
                 [com.wsscode/pathom3 "2023.08.22-alpha" :exclusions [org.clojure/core.async
                                                                       com.fulcrologic/guardrails
                                                                       com.cognitect/transit-cljs]]
                 [org.clojure/core.async "1.6.681"]
                 [com.fulcrologic/guardrails "1.2.5"]
                 [com.github.clj-easy/graal-build-time "1.0.5"]]
  :source-paths ["src"]
  ; :java-source-paths ["java-src"]

  :repl-options {:init-ns user}
  :profiles {:uberjar {:aot :all #_[com.wsscode.pathom3.connect.operation
                                    big-duck.core]
                       :jvm-opts ["-Dclojure.spec.skip-macros=true"
                                  "-Dclojure.compiler.direct-linking=true"]}
             :user {}
             :def {}
             :java {:java-source-paths ["src"]}})
